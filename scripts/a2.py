import logging
import pprint

from scrapy.crawler import CrawlerProcess, Crawler, CrawlerRunner
from scripts.crawler.printerCrawler.spiders.printer import Brother
from twisted.internet import reactor
from scrapy.settings import Settings
from scripts import postprocessing
from collections import defaultdict
from scripts import globalSettings

# Settings for generic site
from scripts import databases

# Crawler laufen lassen?
startCrawler = True
# In Datenbanken eintragen?
insertIntoDatabases = False
# Postprocessing starten?
doPostprocessing = False
# Datenbank leeren?
clearDatabase = False
# Verarbeitung von Error Codes oder/und Kontaktdaten?
errorCodes = True
contacts = False
delete =  True


# Domains fuer Scrappy
mydomains = [
            "brotherprinterrepairs.co.uk"
            # "dasendedesinternet.de",
            # "support.hp.com"
            # "http://www.canon-europe.com/printers/home-printers"
        ]

# URLs fuer Scrappy
myurls = [
            "http://www.brotherprinterrepairs.co.uk/common-errors"
            # "http://www.dasendedesinternet.de",
            # "http://support.hp.com/si-en/document/c01458034",
            # "http://www.canon-europe.com/printers/home-printers"
        ]

##### ERROR CODES #####
if errorCodes:
    # canon, brother oder printertechs
    errorBrand = "brother"

    errorArea = defaultdict(list)
    errorCodeElement = defaultdict(list)
    errorExplanationElement = defaultdict(list)
    errorSolutionElement = defaultdict(list)

    # Check welche Seite
    if(errorBrand == "canon"):
        errorUrl = "http://www.dfelten.de/canon/test.html"
        # area
        errorArea['elem'] = "div"
        errorArea['class'] = "c-full-section-listing"
        # code
        errorCodeElement['elem'] = "h2"
        errorCodeElement['class'] = "c-denda"
        # explanation
        errorExplanationElement['elem'] = "p"
        errorExplanationElement['class'] = "c-detail-introtext"
        # solution
        errorSolutionElement['elem'] = "div"
        errorSolutionElement['class'] = "c-detail-listing-img"

    if(errorBrand == "printertechs"):
        errorUrl = "http://www.printertechs.com/printer-troubleshooting/369-50-1-error-on-hp-laserjet-5000-5100"
        # area
        errorArea['elem'] = "div"
        errorArea['class'] = "item-page"
        # code
        errorCodeElement['elem'] = "h1"
        errorCodeElement['class'] = "bottomline"
        # explanation
        errorExplanationElement['elem'] = "p"
        errorExplanationElement['class'] = ""

    if (errorBrand == "brother"):
        errorUrl = "http://www.brotherprinterrepairs.co.uk/common-errors/"
        # area
        errorArea['elem'] = "div"
        errorArea['class'] = "error_codes"
        # code
        errorCodeElement['elem'] = "p"
        errorCodeElement['class'] = "error_codes_content_sml"

##### CONTACT DATA #####
if contacts:
    contactOrganisation = "TH-Koeln"

    contactArea = defaultdict(list)
    contactNameElement = defaultdict(list)
    contactInfoElement = defaultdict(list)
    contactPhoneElement = defaultdict(list)
    contactEmailElement = defaultdict(list)

    if (contactOrganisation == "TH-Koeln"):
        contactUrl = "https://www.th-koeln.de/hochschule/kontakt_7672.php"
        contactArea['elem'] = "div"
        contactArea['class'] = "inner-box vcard"
        contactNameElement['elem'] = "h2"
        contactNameElement['class'] = ""
        contactInfoElement['elem'] = "p"
        contactInfoElement['class'] = ""
        contactPhoneElement['elem'] = "li"
        contactPhoneElement['class'] = "tel"
        contactEmailElement['elem'] = "li"
        contactEmailElement['class'] = "email"

##### CRAWLER #####
# list of crawlers
TO_CRAWL = [Brother]

# crawlers that are running
RUNNING_CRAWLERS = []

def spider_closing(spider):
    logging.msg("Spider closed: %s" % spider, level=logging.INFO)
    RUNNING_CRAWLERS.remove(spider)
    if not RUNNING_CRAWLERS:
        reactor.stop()

# set up the crawler and start to crawl one spider at a time
def spiderCrawl(domain, url):
    for spider in TO_CRAWL:
        settings = Settings()
        settings.set('BOT_NAME', 'brother')
        settings.set('SPIDER_MODULES', ['printerCrawler.spiders'])
        settings.set('NEWSPIDER_MODULE', 'printerCrawler.spiders')
        settings.set('LOG_LEVEL', 'INFO')
        settings.set('ITEM_PIPELINES', {'printerCrawler.Pipeline.MongoPipeline.MongoPipeline': 100})
        settings.set('MONGODB_SERVER', "192.168.99.100")
        settings.set('MONGODB_PORT', 27017)
        settings.set('MONGODB_DB', '"items"')
        settings.set('MONGODB_COLLECTION', '"printercollection"')
        settings.set('DOWNLOAD_HANDLERS',  {'s3': None})

        runner = CrawlerRunner(settings)
        crawler_obj = spider()
        Brother.allowed_domains = domain
        Brother.start_urls = url
        RUNNING_CRAWLERS.append(crawler_obj)
        d = runner.crawl(crawler_obj)
        d.addBoth(lambda _: reactor.stop())
        reactor.run(0)




def removeDatabaseEntries():
    items = globalSettings.getDatabaseItems()
    items.printercollection.remove({})

# Datenbank leeren
if clearDatabase:
    removeDatabaseEntries()

# Webcrawler starten
if startCrawler:
    spiderCrawl(mydomains, myurls)

# Postprocessing
if doPostprocessing:
    # Holen der Error Codes
    if errorCodes:
        errorDict = postprocessing.GetErrorCodesGeneric(errorBrand, errorUrl, errorArea, errorCodeElement, errorExplanationElement, errorSolutionElement)
        pprint.pprint(errorDict)

        # Eintragen der Error Codes in Datenbanken, sofern diese online sind
        if insertIntoDatabases:
            if databases.isMongoOnline:
                databases.insertErrorCodesIntoMongo(errorDict, delete)
            if databases.isRedisOnline:
                databases.insertErrorCodesIntoRedis(errorDict, delete)
            if databases.isPostgreOnline:
                databases.InsertErrorCodesIntoPostgre(errorDict, delete)
            if databases.isNeo4jOnline:
                databases.insertErrorCodesIntoNeo4J(errorDict, delete)

    # Holen der Kontaktdaten
    if contacts:
        contactDict = postprocessing.GetContactDataGeneric(contactOrganisation, contactUrl, contactArea, contactNameElement, contactInfoElement, contactPhoneElement, contactEmailElement)
        pprint.pprint(contactDict)

        # Eintragen der Kontaktdaten in Datenbanken, sofern diese online sind
        if insertIntoDatabases:
            if databases.isMongoOnline:
                databases.insertContactDataIntoMongo(contactDict, delete)
            if databases.isRedisOnline:
                databases.insertContactDataIntoRedis(contactDict, delete)
            if databases.isPostgreOnline:
                databases.insertContactDataIntoPostgre(contactDict, delete)
            if databases.isNeo4jOnline:
                databases.insertContactDataIntoNeo4J(contactDict, delete)





def martinsTest():
    print("Martins Test")


def martinsZweiterTest(arg):
    print("Martins zweiter test:")
    print(arg)
