from collections import defaultdict
import postprocessing

def existSchema(brand, type):
    if type == "error":
        if (brand == "canon") or (brand == "printertechs") or (brand == "brother"):
            return True
        else:
            return False

    elif type == "contact":
        if (brand == "TH-Koeln"):
            return True
        else:
            return False
    return False

#Holen der vordefinierten Schemata
def doPostprocessing(brand, type):
    if type == "error":
        errorBrand = errorUrl = errorArea = errorCodeElement = errorExplanationElement = errorSolutionElement = ""
        errorArea = defaultdict(list)
        errorCodeElement = defaultdict(list)
        errorExplanationElement = defaultdict(list)
        errorSolutionElement = defaultdict(list)

        if brand == "canon":
            errorBrand = "canon"
            errorUrl = "http://www.dfelten.de/canon/test.html"
            # area
            errorArea['elem'] = "div"
            errorArea['class'] = "c-full-section-listing"
            # code
            errorCodeElement['elem'] = "h2"
            errorCodeElement['class'] = "c-denda"
            # explanation
            errorExplanationElement['elem'] = "p"
            errorExplanationElement['class'] = "c-detail-introtext"
            # solution
            errorSolutionElement['elem'] = "div"
            errorSolutionElement['class'] = "c-detail-listing-img"

        elif brand == "printertechs":
            errorBrand = "printertechs"
            errorUrl = "http://www.printertechs.com/printer-troubleshooting/369-50-1-error-on-hp-laserjet-5000-5100"
            # area
            errorArea['elem'] = "div"
            errorArea['class'] = "item-page"
            # code
            errorCodeElement['elem'] = "h1"
            errorCodeElement['class'] = "bottomline"
            # explanation
            errorExplanationElement['elem'] = "p"
            errorExplanationElement['class'] = ""

        elif brand == "brother":
            errorBrand = "brother"
            errorUrl = "http://www.brotherprinterrepairs.co.uk/common-errors/"
            # area
            errorArea['elem'] = "div"
            errorArea['class'] = "error_codes"
            # code
            errorCodeElement['elem'] = "p"
            errorCodeElement['class'] = "error_codes_content_sml"

        return postprocessing.GetErrorCodesGeneric(errorBrand, errorUrl, errorArea, errorCodeElement,
                                                   errorExplanationElement, errorSolutionElement)

    elif type == "contact":
        contactOrganisation = contactUrl = contactArea = contactNameElement = contactInfoElement= contactPhoneElement = contactEmailElement = ""
        contactArea = defaultdict(list)
        contactNameElement = defaultdict(list)
        contactInfoElement = defaultdict(list)
        contactPhoneElement = defaultdict(list)
        contactEmailElement = defaultdict(list)

        if brand == "TH-Koeln":
            contactOrganisation = "TH-Koeln"

            contactUrl = "https://www.th-koeln.de/hochschule/kontakt_7672.php"
            contactArea['elem'] = "div"
            contactArea['class'] = "inner-box vcard"
            contactNameElement['elem'] = "h2"
            contactNameElement['class'] = ""
            contactInfoElement['elem'] = "p"
            contactInfoElement['class'] = ""
            contactPhoneElement['elem'] = "li"
            contactPhoneElement['class'] = "tel"
            contactEmailElement['elem'] = "li"
            contactEmailElement['class'] = "email"

        return postprocessing.GetContactDataGeneric(contactOrganisation, contactUrl, contactArea,
                                                           contactNameElement, contactInfoElement, contactPhoneElement,
                                                           contactEmailElement)