import datetime
import time
import os

from scrapy.core import engine
from scrapy.signals import spider_closed
from scrapy.http import Request
from scrapy.spiders import CrawlSpider, Rule
from scrapy.crawler import Crawler
from scrapy.linkextractors import LinkExtractor
from crawler.printerCrawler.items import PrinterItem
from crawler.printerCrawler.spiders import errorCodeTags
import urllib, urllib2

"""
ToDo: DomTree Durchlaufen, Pipeline einmal vonn A-Z durchprobieren, Dokumentation
"""

class Brother(CrawlSpider):
    post_url ="http://localhost:8000/a2/crawler/crawlerResult/create/"

    name = "brother"
    rules = (
        # Extract links matching 'item.php' and parse them with the spider's method parse_item
        Rule(LinkExtractor(allow=(".",), deny=("javascript", "#")), callback='parse_item', follow=True),
    )

    def findErrorCode(self, body):
        print("----Neue Seite----")
        for errorCodeTag in errorCodeTags.getErrorCodeTags():
            print(errorCodeTag)
            liste = body.xpath('//*[contains(text(),"' + errorCodeTag + '")]')
            if liste:
                return True
        return False

    def parse_item(self, response):
        # self.logger.info('Hi, this is an item page! %s', response.url)
        item = PrinterItem()
        item['url'] = response.url
        item['body'] = response.body
        item['timestamp'] = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        if self.findErrorCode(response.xpath('//*')) == True:
            print response.url
            # self.sendToDjango(item)
            yield item

    def parse_start_url(self, response):
        return Request("http://www.dfelten.de", callback=self.parse_item, )

    def sendToDjango(self, item):
        data = urllib.urlencode(item)
        request = urllib2.Request(self.post_url, data)
        response = urllib2.urlopen(request)
        html = response.read()
        print "LOLOLOL"
        print html
