# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class PrinterItem(scrapy.Item):
    url = scrapy.Field()
    body = scrapy.Field()
    hash = scrapy.Field()
    timestamp = scrapy.Field()