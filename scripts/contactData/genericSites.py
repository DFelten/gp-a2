# coding=utf-8
from pymongo import MongoClient
from bs4 import BeautifulSoup
from collections import defaultdict
from scripts import globalSettings

#Datenbankzugriff
db = globalSettings.getDatabaseItems()

#Error-Codes anhand eines Bereiches und Klassen der HTML-Elemente ermitteln
def GetContactData(organisation, url, area, nameElement, infoElement, telefonElement, emailElement):
    collection = db.testCollection
    kontaktList = defaultdict(list)
    #get all entrys from the database with the given url
    entries = collection.find({"url": url})
    for entry in entries:
        soup = BeautifulSoup(entry["html"], 'html.parser')

        KontaktCodesClasses = soup.findAll(area['elem'], {"class": area['class']})
        for KontaktCodesClass in KontaktCodesClasses:
            kontaktElmentList = defaultdict(list)
            # kontaktElmentList["Organisation"].append("TH-Koeln")
            name = KontaktCodesClass.find(nameElement['elem'])
            if name:
                name = name.string
                kontaktElmentList["Name"] = name.string

            adresse = KontaktCodesClass.find(infoElement['elem'])
            if adresse:
                kontaktElmentList["Info/Adresse"] = adresse.get_text('\n', strip=True)

            telefon = KontaktCodesClass.find(telefonElement['elem'], {"class": telefonElement['class']})
            if telefon:
                telefon = telefon.find("span").string
                kontaktElmentList["Telefon"] = telefon
                print (telefon)

            email = KontaktCodesClass.find(emailElement['elem'], {"class": emailElement['class']})
            if email:
                email = email.find("span").string
                kontaktElmentList["E-Mail"] = email
            kontaktList[name] = kontaktElmentList

    resultlist = defaultdict(list)
    resultlist['organisation'] = organisation
    resultlist["contacts"].append(kontaktList)
    return resultlist
