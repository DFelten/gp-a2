from pymongo import MongoClient
from bs4 import BeautifulSoup
from collections import defaultdict
from scripts import globalSettings

#Datenbankzugriff
db = globalSettings.getDatabaseItems()
print db
collection = db.testCollection

def getContactdata():
    print "### getContactDATA"
    contactList = defaultdict(dict)
    entries = collection.find({"html": {"$exists": "true"}})
    for entry in entries:
        soup = BeautifulSoup(entry["html"], 'html.parser')
        KontaktCodesClasses = soup.findAll("div", {"class": "inner-box vcard"})
        for KontaktCodesClass in KontaktCodesClasses:
            kontaktElmentList = defaultdict(dict)
            name = KontaktCodesClass.find("h2")
            if name:
                name = name.string
                kontaktElmentList["Name"]= (name.string)
            adresse = KontaktCodesClass.find("p")
            if adresse:
                kontaktElmentList["Info/Adresse"] = (adresse.get_text('\n', strip=True))

            telefon = KontaktCodesClass.find("li", {"class": "tel"})
            if telefon:
                telefon = telefon.find("span").string
                kontaktElmentList["Telefon"]= (telefon)

            email = KontaktCodesClass.find("li", {"class": "email"})

            if email:
                email = email.find("span").string
                kontaktElmentList["E-Mail"]=(email)
            contactList[name]=(kontaktElmentList)

    resultlist = defaultdict(dict)
    resultlist["organisation"] = "TH-Koeln"
    resultlist["contacts"] = (contactList)
    return resultlist
