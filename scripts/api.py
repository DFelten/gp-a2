# from scripts import databases
# from gui.a2.models import BenchmarkResultRecord
import time, random, threading
import handler
# from scripts import a2
import benchmarking

import os
from sys import path
from os.path import dirname

from scrapy.core import engine
from scrapy.exceptions import CloseSpider

path.append(dirname(dirname(os.path.realpath(__file__)) + '\\crawler\\'))



def start_benchmark(benchmark):
    print "api.start_benchmark()"
    crawlerThread = threading.Thread(target=prepare_start_benchmark, args=(benchmark,), daemon=True)
    crawlerThread.start()


def stopCrawling():
    print "API.stopCrawling"
    crawlerThread = threading.Thread(target=handler.spider_closing, args=())
    crawlerThread.start()

    # spider = handler.getRunning()
    # handler.spider_closing(spider)
    # raise CloseSpider('bandwidth_exceeded')

    # print spider.name
    # spider.close(spider,"reasons")


def prepare_start_benchmark(benchmark):
    print ("Starte Benchmark durch aPI \n")
    # benchie = benchmarking.Benchmarking
    # benchie.generateTestData('brother', True)
    # benchie.prepareErrorDict()
    # list = []
    # if benchmark.useMongoDB: list.append('mongo')
    # if benchmark.useRedis: list.append('redis')
    # if benchmark.usePostgres: list.append('postgre')
    # # if benchmark.neo4j: list.append('neo4j')
    # obergrenze = benchmark.factor
    # wiederholungen = 1
    # operationen = []
    # operationen.append('lesen')
    # operationen.append('schreiben')
    # operationen.append('loeschen')
    # benchie.benchmark(list, obergrenze, wiederholungen, operationen)

def start_crawler(crawler, settings):
    print "API.start_crawler"
    print crawler
    if crawler.entitytyp == 'Error Codes':
        crawlerThread = threading.Thread(target=getErrorCode, args=(crawler, settings))
        crawlerThread.start()
    elif crawler.entitytyp == 'Contact Details':
        # Mapper
        cdc = {}
        cdc['insertIntoDatabases'] = crawler.insertIntoDatabases
        cdc['startCrawler'] = crawler.startCrawler
        cdc['itemDomains'] = [crawler.mydomains]
        cdc['itemURLs'] = [crawler.myurls]
        cdc['deleteOldEntries'] = crawler.clearDatabase
        # Settings Part
        cdc['contactOrganisation'] = settings.attr02
        cdc['contactUrl'] = settings.attr01
        cdc['contactArea'] = settings.attr03
        cdc['contactNameElement'] = settings.attr05
        cdc['contactInfoElement'] = settings.attr07
        cdc['contactPhoneElement'] = settings.attr09
        cdc['contactEmailElement'] = settings.attr11
        import handler
        handler.startContactProcess(
            cdc['insertIntoDatabases'], cdc['startCrawler'], cdc['contactOrganisation'], cdc['contactUrl'],
            cdc['deleteOldEntries'], cdc['contactOrganisation'], cdc['contactUrl'], cdc['contactArea'],
            cdc['contactNameElement'], cdc['contactInfoElement'], cdc['contactPhoneElement'], cdc['contactEmailElement']
        )
        print ("CD")
        print (crawler)
    else:
        print ("None of Those - greetings from the api")


def getErrorCode(crawler, settings):
    print ("EC")
    # Mapper
    ecc = {}
    ecc['insertIntoDatabases'] = crawler.insertIntoDatabases
    ecc['startCrawler'] = crawler.startCrawler
    ecc['itemDomains'] = [crawler.mydomains]
    ecc['itemURLs'] = [crawler.myurls]
    ecc['deleteOldEntries'] = crawler.clearDatabase
    # Settings Part
    ecc['errorUrl'] = settings.attr01
    ecc['errorBrand'] = settings.attr02
    ecc['errorArea'] = settings.attr03
    ecc['errorCodeElement'] = settings.attr05
    ecc['errorExplanationElement'] = settings.attr07
    ecc['errorSolutionElement'] = settings.attr09
    print ecc
    import handler

    #handler.spiderCrawl(["crawler.myurls"], ["crawler.mydomains"])
    handler.startErrorCodeProcess(
        ecc['insertIntoDatabases'], ecc['startCrawler'], ecc['itemDomains'], ecc['itemURLs'],
        ecc['deleteOldEntries'], ecc['errorBrand'], ecc['errorUrl'], ecc['errorArea'],
        ecc['errorCodeElement'], ecc['errorExplanationElement'], ecc['errorSolutionElement']
    )


def get_benchmark_record(benchmarkResult):
    records = createDummyRecordsForBenchmarks(benchmarkResult)
    return records


def get_crawler_record(crawler_result):
    records = createDummyRecordsForCrawler(crawler_result)

    # magic api stuff
    return records


def get_database_availability():
    print ("api.get_database_availability")
    return (
        {'name':'MongoDB','online':False}, # databases.isMongoOnline()
        {'name':'Neo4J','online':False}, # databases.isNeo4jOnline()
        {'name':'Postgres','online':False}, # databases.isPostgreOnline()
        {'name':'Redis','online':False}, # databases.isRedisOnline()
    )

def createDummyRecordsForBenchmarks(benchmarkResult):
    import time
    DATABASES = ['MongoDB', 'Postgres', 'Neo4J', 'Redis', ]
    TYPES = ['Mean','StdDev','Multiple','Linear']
    OPERATIONS = ['Insert', 'Delete']
    records = []
    benchmark = benchmarkResult.benchmark
    # print benchmark
    for db in DATABASES:
        for typ in TYPES:
            for operation in OPERATIONS:
                record = {'created': benchmarkResult.created,
                          'types':typ,
                          'databases':db,
                          'operations':operation,
                          'benchmarkResult':benchmarkResult,
                          'amount':random.randrange(0, 10000, 1),
                          'time':random.randrange(0, 10000, 1)
                          }
                records.append(record)
    return records


def createDummyRecordsForCrawler(crawler_result):
    records = []
    # print benchmark
    i = 0
    while i < 2:
        record = {'crawlerResult': crawler_result,
                  'url': 'http://www.google.de/',
                  'created': time.time()
                  }
        print (record)
        records.append(record)
        i += 1
    return records