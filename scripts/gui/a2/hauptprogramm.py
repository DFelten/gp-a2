import threading

# if path.append(dirname(abspath(__file__))) not in path:
#     path.append(dirname(abspath(__file__)))
#     print(path)

threads = {}


def handler(case, obj):
    print ("Hauptprogramm: Handler")
    print case

    if case == 1:
        print ("Hauptprogramm: Handler2: Crawler Start Thread started")
        crawlerThread = threading.Thread(target=crawler_start, args=(obj,))
        crawlerThread.start()
    elif case == 2:
        print ("Hauptprogramm: Handler2: Crawler Finish Thread started")
        crawlerThread = threading.Thread(target=crawler_finish, args=(obj,))
        crawlerThread.start()
    elif case == 3:
        print ("Hauptprogramm: Handler2: benchmark_start Thread started")
        crawlerThread = threading.Thread(target=benchmark_start, args=(obj,))
        crawlerThread.start()
    elif case == 4:
        print ("Hauptprogramm: Handler2: benchmark_finish Thread started")
        crawlerThread = threading.Thread(target=benchmark_finish, args=(obj,))
        crawlerThread.start()
    else:
        print "nothing to do"