# coding=utf-8
from django.http import HttpResponseRedirect, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, get_object_or_404, redirect

from .models import Crawler, Benchmark,  CrawlerResult, BenchmarkResultRecord, CrawlerResultRecord,CrawlerSettings
from .forms import CrawlerForm, BenchmarkForm, BenchmarkResult, CrawlerSettingsForm
from django.contrib import messages
from os.path import dirname, abspath
# from scripts import api
from sys import path
import sys, time
from scripts import globalSettings

# Dokumentation
if path.append(dirname(abspath(__file__))) not in path:
    path.append(dirname(abspath(__file__)))
    path.append(dirname(dirname(abspath(__file__))))
    path.append(dirname(dirname(dirname(abspath(__file__)))))
    print(path)


def crawler_create(request):
    form = CrawlerForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.isrunning = False
        instance.save()
        return HttpResponseRedirect(instance.create_settings())
    context = {
        "form": form,
        "create": "true"
    }
    return render(request, 'crawler/crawler_form.html', context)


def crawler_detail(request, id):  # retrieve
    instance = get_object_or_404(Crawler, id=id)
    settings = None
    try:
        settings = CrawlerSettings.objects.filter(crawler=id)[0]
    except:
        settings = None

    if settings != None:
        context = {
            "title": "detail",
            "instance": instance,
            "settings": settings
        }
    else:
        context = {
            "title": "detail",
            "instance": instance
        }
    return render(request, 'crawler/crawler_detail.html', context)


def crawler_list(request):
    crawlerQueryset = Crawler.objects.all()
    crawlerResultQueryset = CrawlerResult.objects.all()
    context = {
        "object_list": crawlerQueryset,
        "crawler_result_object_list": crawlerResultQueryset,
        "title": "LISTE"
    }
    return render(request, 'crawler/crawler_list.html', context)


def crawler_update(request, id=None):
    instance = get_object_or_404(Crawler, id=id)
    form = CrawlerForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        # messages.success(request, "<a href='" + instance.get_absolute_url() + "edit/'>item updated</a>",
        #                  extra_tags='html_safe')
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "title": "detail",
        "instance": instance,
        "form": form,
    }
    return render(request, 'crawler/crawler_form.html', context)


def crawler_delete(request, id=None):
    instance = get_object_or_404(Crawler, id=id)
    instance.delete()
    # messages.success(request, "successfully deleted")
    return redirect("a2:crawler_list")


@csrf_exempt
def crawler_start(request, id):
    import api
    Crawler.objects.filter(pk=id).update(isrunning=True)
    crawler = Crawler.objects.filter(pk=id)[0]
    settings = CrawlerSettings.objects.filter(crawler=crawler.id)[0]
    api.start_crawler(crawler, settings)
    return HttpResponseRedirect("http://localhost:8000/a2/crawler/")


# aufgerufen vom template
@csrf_exempt
def crawler_stop(request, id):
    Crawler.objects.filter(pk=id).update(isrunning=False)
    import api
    api.stopCrawling()
    return HttpResponseRedirect("http://localhost:8000/a2/crawler/")


@csrf_exempt
def crawler_finish(request, id):
    Crawler.objects.filter(pk=id).update(isrunning=False)
    # return HttpResponse(status=200)
    return render("http://localhost:8000/a2/crawler/")


def benchmark_create(request):
    form = BenchmarkForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        new_benchmark = form.save(commit=False)
        new_benchmark.save()
        # messages.success(request, "successfully created")
        return HttpResponseRedirect(new_benchmark.get_absolute_url())
    context = {
        "form": form,
        "create": "true"
    }
    return render(request, 'benchmark/Benchmark_form.html', context)


def benchmark_detail(request, id):  # retrieve
    instance = get_object_or_404(Benchmark, id=id)
    context = {
        "title": "Detailansicht",
        "instance": instance,
    }
    return render(request, 'benchmark/Benchmark_detail.html', context)


def benchmark_list(request):
    import api
    availability = api.get_database_availability()
    results = BenchmarkResult.objects.all()
    queryset = Benchmark.objects.all()
    context = {
        "availability": availability,
        "results": results,
        "object_list": queryset,
        "title": "Liste vorhandener Benchmarks"
    }
    return render(request, 'benchmark/Benchmark_list.html', context)


def benchmark_update(request, id=None):
    instance = get_object_or_404(Benchmark, id=id)
    form = BenchmarkForm(request.POST or None, request.FILES or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        # messages.success(request, "<a href='" + instance.get_absolute_url() + "edit/'>item updated</a>",
        #                  extra_tags='html_safe')
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "title": "detail",
        "instance": instance,
        "form": form,
    }
    return render(request, 'benchmark/Benchmark_form.html', context)


def benchmark_delete(request, id=None):
    instance = get_object_or_404(Benchmark, id=id)
    instance.delete()
    # messages.success(request, "successfully deleted")
    return redirect("a2:benchmark_list")


@csrf_exempt
def benchmark_result_detail(request, id):
    instance = get_object_or_404(BenchmarkResult, id=id)
    records = BenchmarkResultRecord.objects.filter(benchmarkResult=instance)
    context = {
        "title": "Detailansicht",
        "instance": instance,
        "records":records,
    }
    return render(request, 'benchmark/benchmark_result_detail.html', context)


@csrf_exempt
def benchmark_start(request, id):
    import api
    benchmark = Benchmark.objects.filter(pk=id)[0]
    api.start_benchmark(benchmark)
    benchmark_result = benchmarkResult_create(benchmark)
    benchmark_result_record_create(benchmark_result)
    try:
        print("benchmark_start")
        # hauptprogramm.handler(3, args=Benchmark.objects.filter(pk=id))
    except:
        print ("Unexpected error:", sys.exc_info()[0])
        print ("Error: unable to start thread")
    return HttpResponseRedirect("http://localhost:8000/a2/benchmark/")


@csrf_exempt
def benchmarkResult_create(benchmark):
    benchmark_result = BenchmarkResult(name=benchmark.name, created=time.time(), benchmark=benchmark)
    benchmark_result.save()
    return benchmark_result


@csrf_exempt
def benchmark_result_record_create(benchmark_result):
    import api
    records = api.get_benchmark_record(benchmark_result)
    for item in records:
        brr = BenchmarkResultRecord(
            created = item['created'],
            types = item['types'],
            databases = item['databases'],
            operations = item['operations'],
            benchmarkResult = benchmark_result,
            amount = item['amount'],
            time = item['time'],
        )
        brr.save()
    return records


# aufgerufen vom template
@csrf_exempt
def benchmark_stop(request, id):
    # hauptprogramm.handler(5, Crawler.objects.get(pk=id))
    Benchmark.objects.filter(pk=id).update(isrunning=False)
    return HttpResponseRedirect("http://localhost:8000/a2/benchmark/")


@csrf_exempt
def benchmark_finish(request, id):
    # hauptprogramm.handler(4, Crawler.objects.get(pk=id))
    Benchmark.objects.filter(pk=id).update(isrunning=False)
    return render("http://localhost:8000/a2/benchmark/")

# ------------------- NEW PART


@csrf_exempt
def benchmarkResult_delete(request, id):
    instance = get_object_or_404(BenchmarkResult, id=id)
    instance.delete()
    # messages.success(request, "successfully deleted")
    return redirect("a2:benchmark_list")


@csrf_exempt
def crawler_result_create(request):
    print "########################"
    print request
    crawler = Crawler.objects.filter(isrunning=True)[0]
    if crawler:
        print crawler
    else:
        print "no crawler"
    crawler_result = CrawlerResult.objects.filter(crawler=crawler)[0]
    if crawler_result:
        print crawler_result
    print "no crawlerREsult"
    print crawler_result.created
    crawler_result.save()
    return HttpResponse(status=200)


@csrf_exempt
def crawlerResult_delete(request, id):
    instance = get_object_or_404(CrawlerResult, id=id)
    instance.delete()
    # messages.success(request, "successfully deleted")
    return redirect("a2:crawler_list")


@csrf_exempt
def crawler_result_detail(request, id):
    instance = get_object_or_404(CrawlerResult, id=id)
    records = CrawlerResultRecord.objects.filter(crawlerResult=instance)
    context = {
        "title": "Detailansicht",
        "instance": instance,
        "records": records,
    }
    return render(request, 'crawler/crawler_result_detail.html', context)


@csrf_exempt
def crawler_result_record_create(crawler_result):
    from scripts import api
    records = api.get_crawler_record(crawler_result)
    for item in records:
        crr = CrawlerResultRecord(
            crawlerResult = item['crawlerResult'],
            url = item['url'],
            created = item['created']
        )
        crr.save()
    return records


def crawler_settings_create(request, id):
    crawler = Crawler.objects.filter(pk=id)[0]
    form = CrawlerSettingsForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        crawler_settings = form.save(commit=False)
        crawler_settings.crawler = crawler.id
        crawler_settings.save()
        return HttpResponseRedirect("http://localhost:8000/a2/crawler")
    else:
        "unvalid"
    context = {
        "form": form,
        "crawler": crawler,
        "create": "true"
    }
    return render(request, 'crawler/crawler_settings_form.html', context)


def crawler_settings_update(id):
    print ("crawler_settings_create, crawler_settings_delete, crawler_settings_update")

def crawler_settings_delete(id):
    print ("crawler_settings_create, crawler_settings_delete, crawler_settings_update")


# ueberarbeiten
def crawler_settings_detail(request, id):
    print ("CRAWLER SETTINGS")
    instance = get_object_or_404(CrawlerSettings, pk=id)
    context = {
        "title": "detail",
        "instance": instance,
    }
    return render(request, 'crawler/crawler_settings_detail.html', context)


@csrf_exempt
def api_crawler_result_record_create(request,):

    # crr = CrawlerResultRecord(
    #     crawlerResult = item['crawlerResult'],
    #     url = item['url'],
    #     created = item['created']
    # )
    # crr.save()
    return HttpResponse(status=200)


@csrf_exempt
def api_benchmark_result_record_create(request):
    return HttpResponse(status=200)


