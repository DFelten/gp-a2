"""djangogui URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from .views import (
benchmark_list, benchmark_create, benchmark_detail, benchmark_update, benchmark_delete,benchmark_start, benchmark_finish,benchmark_stop,
crawler_list, crawler_create, crawler_delete, crawler_update, crawler_detail, crawler_stop, crawler_start, crawler_finish,
benchmarkResult_create, benchmarkResult_delete,benchmark_result_detail,
crawler_result_create, crawlerResult_delete,  crawler_result_detail,
crawler_settings_create, crawler_settings_delete, crawler_settings_update, crawler_settings_detail,
)

app_name = 'a2'
urlpatterns = [
    url(r'^benchmark/$', benchmark_list, name='benchmark_list'),
    url(r'^benchmark/create/', benchmark_create, name='benchmark_create'),
    url(r'^benchmark/(?P<id>\d+)/$', benchmark_detail, name='benchmark_detail'),
    url(r'^benchmark/(?P<id>\d+)/edit/$', benchmark_update, name='benchmark_update'),
    url(r'^benchmark/(?P<id>\d+)/delete/$', benchmark_delete, name='benchmark_delete'),
    url(r'^benchmark/(?P<id>\d+)/start/$', benchmark_start, name='benchmark_start'),
    url(r'^benchmark/(?P<id>\d+)/finish/$', benchmark_finish, name='benchmark_finish'),
    url(r'^benchmark/(?P<id>\d+)/stop/$', benchmark_stop, name='benchmark_stop'),
    url(r'^benchmark/result_create/$', benchmarkResult_create, name='benchmarkResult_create'),
    url(r'^benchmark/result_detail/(?P<id>\d+)/delete/$', benchmarkResult_delete, name='benchmarkResult_delete'),
    url(r'^benchmark/result_detail/(?P<id>\d+)/$', benchmark_result_detail, name='benchmark_result_detail'),
    # url(r'^benchmark/result/$', benchmarkResult_delete, name='benchmarkResult_delete'),

    url(r'^crawler/$', crawler_list, name='crawler_list'),
    url(r'^crawler/create/', crawler_create, name='crawler_create'),
    url(r'^crawler/(?P<id>\d+)/$', crawler_detail, name='crawler_detail'),
    url(r'^crawler/(?P<id>\d+)/edit/$', crawler_update, name='crawler_update'),
    url(r'^crawler/(?P<id>\d+)/delete/$', crawler_delete, name='crawler_delete'),
    url(r'^crawler/(?P<id>\d+)/start/$', crawler_start, name='crawler_start'),
    url(r'^crawler/(?P<id>\d+)/stop/$', crawler_stop, name='crawler_stop'),
    url(r'^crawler/(?P<id>\d+)/finish/$', crawler_finish, name='crawler_finish'),

    url(r'^crawler/crawlerResult/create/', crawler_result_create, name='crawler_result_create'),
    url(r'^crawler/crawlerResult/(?P<id>\d+)/delete/$', crawlerResult_delete, name='crawlerResult_delete'),
    url(r'^crawler/crawlerResult/(?P<id>\d+)/$', crawler_result_detail, name='crawler_result_detail'),
    url(r'^crawler/(?P<id>\d+)/crawlerSettings/create/$', crawler_settings_create, name='crawler_settings_create'),
    url(r'^crawler/(?P<id>\d+)/crawlerSettings/delete/$', crawler_settings_delete, name='crawler_settings_delete'),
    url(r'^crawler/(?P<id>\d+)/crawlerSettings/edit/$', crawler_settings_update, name='crawler_settings_update'),
    url(r'^crawler/(?P<id>\d+)/crawlerSettings/$', crawler_settings_detail, name='crawler_settings_detail'),


]