from django import forms
from django.forms import ModelForm
from .models import Crawler, Benchmark, BenchmarkResult, CrawlerResult, \
    BenchmarkResultRecord, CrawlerResultRecord, CrawlerSettings
# Dokumentation
# Damit Django erkennt, welche Felder 'required' sind, muss es deklariert werden, importiert werden alle Felder im Model
# fields = ('name', 'starturl')
# shortcut fuer alle felder
#http://timlwhite.com/2012/07/21/django-many-to-many-model-saving-with-intermediary-through-model/


class CrawlerForm(ModelForm):
    class Meta:
        model = Crawler
        fields = '__all__'
        exclude = ['isrunning', ]


class BenchmarkForm(ModelForm):
    class Meta:
        model = Benchmark
        fields = '__all__'
        exclude = ['isrunning']


class BenchmarkResultForm(ModelForm):
    class Meta:
        model = BenchmarkResult
        fields = '__all__'


class CrawlerResultForm(forms.ModelForm):
    class Meta:
        model = CrawlerResult
        fields = '__all__'


class CrawlerResultForm(forms.ModelForm):
    class Meta:
        model = CrawlerResultRecord
        fields = '__all__'


class BenchmarkResultForm(forms.ModelForm):
    class Meta:
        model = BenchmarkResultRecord
        fields = '__all__'


class CrawlerSettingsForm(forms.ModelForm):
    class Meta:
        model = CrawlerSettings
        fields = '__all__'
        exclude = ['Crawler']