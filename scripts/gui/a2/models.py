# coding=utf-8
from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse


class Crawler (models.Model):
    ENTITIES = (('Error Codes','Error Codes'),('Contact Details','Contact Details'))
    # BRANCHES = (('BR','Brother'),('HP','Hewlett Pakard'),('CA','Canon'))
    name = models.CharField(max_length=100)
    entitytyp = models.CharField(max_length=250, choices=ENTITIES)
    startCrawler= models.BooleanField()
    insertIntoDatabases = models.BooleanField()
    doPostprocessing = models.BooleanField()
    clearDatabase = models.BooleanField()
    mydomains = models.CharField(max_length=255, )
    myurls = models.CharField(max_length=255, )
    # 2 Möglichkeiten: 1. Contact Details 2. Error Codes
    isrunning = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("a2:crawler_detail", kwargs={"id": self.id})

    def get_settings(self):
        return reverse("a2:crawler_settings_detail", kwargs={"id": self.id})

    def create_settings(self):
        return reverse("a2:crawler_settings_create", kwargs={"id": self.id})

class CrawlerResult(models.Model):
    crawler = models.ForeignKey(Crawler)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    totalpages = models.CharField(max_length=100)
    crawledpages = models.CharField(max_length=10)

    def __unicode__(self):
        return self.crawler.name

    def __str__(self):
        return self.crawler.name

    def get_crawler(self):
        return self.crawler

    def get_absolute_url(self):
        return reverse("a2:crawler_result_detail", kwargs={"id": self.id})


class CrawlerResultRecord(models.Model):
    crawlerResult = models.ForeignKey(CrawlerResult)
    url = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.name

    def get_url(self):
        return self.url

    def get_created(self):
        return self.created


class CrawlerSettings(models.Model):
    crawler = models.PositiveSmallIntegerField(default=1, null=True, blank=True)
    attr01 = models.CharField(max_length=100,blank=True,null=True)
    attr02 = models.CharField(max_length=100,blank=True,null=True)
    attr03 = models.CharField(max_length=100,blank=True,null=True)
    attr04 = models.CharField(max_length=100,blank=True,null=True)
    attr05 = models.CharField(max_length=100,blank=True,null=True)
    attr06 = models.CharField(max_length=100,blank=True,null=True)
    attr07 = models.CharField(max_length=100,blank=True,null=True)
    attr08 = models.CharField(max_length=100,blank=True,null=True)
    attr09 = models.CharField(max_length=100,blank=True,null=True)
    attr10 = models.CharField(max_length=100,blank=True,null=True)
    attr11 = models.CharField(max_length=100,blank=True,null=True)
    attr12 = models.CharField(max_length=100,blank=True,null=True)



# BENCHMARK


class Benchmark(models.Model):
    ENTITIES = (('EC', 'Error Codes'), ('CD', 'Contact Details'))
    name = models.CharField(max_length=120)
    dataset = models.CharField(max_length=2, choices=ENTITIES)
    isrunning = models.BooleanField(default=False)
    factor = models.PositiveSmallIntegerField(default=1, null=False)
    read_ratio = models.PositiveSmallIntegerField(default=1, null=False)
    write_ratio = models.PositiveSmallIntegerField(default=1, null=False)
    useMongoDB = models.BooleanField(default=True)
    useNoe4J = models.BooleanField(default=True)
    useRedis = models.BooleanField(default=True)
    usePostgres = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("a2:benchmark_detail", kwargs={"id": self.id})

    class Meta:
        ordering = ('created',)


class BenchmarkResult(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    benchmark = models.ForeignKey(Benchmark)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name

    def get_created(self):
        return self.created

    def get_benchmark(self):
        return self.benchmark

    def get_absolute_url(self):
        # return ("http://localhost:8000/a2/benchmark/")
        return reverse("a2:benchmark_result_detail", kwargs={"id": self.id})


class BenchmarkResultRecord(models.Model):
    TYPES = (('Mean','Mean'), ('StdDev','StdDev'), ('Multiple','Multiple'), ('Linear','Linear'))
    DATABASES = (('MongoDB', 'MongoDB'), ('Postgres', 'Postgres'), ('Neo4J', 'Neo4J'), ('Redis', 'Redis'))
    OPERATIONS = (('Insert', 'Insert'), ('Delete', 'Delete'))
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    types = models.CharField(max_length=25, choices=TYPES)
    databases = models.CharField(max_length=25, choices=DATABASES)
    operations = models.CharField(max_length=25, choices=OPERATIONS)
    benchmarkResult = models.ForeignKey(BenchmarkResult)
    amount = models.IntegerField()
    time = models.IntegerField()

    def get_created(self):
        return self.created

    def get_benchmark_result_set(self):
        return self.benchmarkResult