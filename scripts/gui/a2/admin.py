from django.contrib import admin

from .models import Benchmark, Crawler


class BenchmarkAdmin(admin.ModelAdmin):
    list_display = ["name", "created"]
    list_display_links = ["name"]
    list_filter = ["name","created"]
    search_fields = ["name"]
    class Meta:
        model = Benchmark


class CrawlerAdmin(admin.ModelAdmin):
    list_display = ["name", "myurls"]
    list_display_links = ["name"]
    list_filter = ["name"]
    search_fields = ["name"]
    class Meta:
        model = Crawler

admin.site.register(Crawler, CrawlerAdmin)
admin.site.register(Benchmark, BenchmarkAdmin)