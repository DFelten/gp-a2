# coding=utf-8
from pymongo import MongoClient
import urllib
import errorCodes.brother
import errorCodes.canon
import errorCodes.genericSites
import contactData.thpage
import contactData.genericSites
import contactData.orgadata
from collections import defaultdict
import globalSettings

#Datenbankzugriff
db = globalSettings.getDatabaseItems()

#Einfügen eines Eintrags in die MongoDB-Datenbank
def InsertIntoDB(link):
    f = urllib.urlopen(link)
    myFile = f.read()
    html = myFile.decode('utf8')
    db.printercollection.insert({"body": html, "url": link})

#Funktion zum Erhalten von Errorcodes
def GetErrorCodes(type):
    if (type == "brother"):
        #InsertIntoDB("http://www.brotherprinterrepairs.co.uk/common-errors/")
        return errorCodes.brother.GetErrorCodes()
    elif (type == "canon"):
        #InsertIntoDB("http://www.canon.de/support/consumer_products/product_ranges/printers/pixma/error_codes/")
        return errorCodes.canon.GetErrorCodes()

    # Empty list
    return defaultdict(list)

#Funktion zum "generischen" Erhalten von errorCodes
def GetErrorCodesGeneric(brand, url, area, codeElement, explanationElement, solutionElement):
    #InsertIntoDB("http://www.brotherprinterrepairs.co.uk/common-errors/")
    return errorCodes.genericSites.GetErrorCodes(brand, url, area, codeElement, explanationElement, solutionElement)

#Funktion zum Erhalten von Kontaktdaten
def GetContactData(type):
    # Empty list
    print "### GET CONTACT DATA"
    if(type == "thpage"):
        InsertIntoDB("https://www.th-koeln.de/hochschule/kontakt_7672.php")
        return contactData.thpage.getContactdata()
    if (type == "orgadata"):
        # InsertIntoDB("http://www.orgadata.com/unternehmen/kontakt/vertrieb-international.html")
        return contactData.orgadata.getContactdata()
    return  defaultdict(list)

#Funktion zum "generischen" Erhalten von Kontaktdaten
def GetContactDataGeneric(organisation, url, area, nameElement, infoElement, telefonElement, emailElement):
    return contactData.genericSites.GetContactData(organisation, url, area, nameElement, infoElement, telefonElement, emailElement)
