# coding=utf-8
from pymongo import MongoClient


def getDatabaseItems():
    client = MongoClient(getMongoIP(), 27017)
    # print "########"
    # print client
    return client.items


def getMongoURI(): return '192.168.99.100:27017'


def getNeo4jURI(): "http://neo4j:123456@" + getNeo4jIP() + ":7474/db/data/"


def getDatabasePrep(): return 'items'


def getMongoIP(): return '192.168.99.100'


def getRedisIP(): return '192.168.99.100'


def getNeo4jIP(): return '192.168.99.100'


def getPostgreIP(): return '192.168.99.100'


def getMongoErrorCodeCollection():
    db = getDatabaseItems()
    return db.printercollection
