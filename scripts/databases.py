import json
import time

from psycopg2._psycopg import OperationalError
from py2neo.packages.httpstream import SocketError
from pymongo.errors import ServerSelectionTimeoutError
from redis.exceptions import ConnectionError
from pymongo import MongoClient
import redis
from py2neo import Graph, Path, authenticate, Node, Relationship
import globalSettings
import psycopg2
from psycopg2 import extras
from copy import deepcopy

def isMongoOnline():
    client = MongoClient(globalSettings.getMongoIP(), 27017, serverSelectionTimeoutMS=3000)
    try:
        client.address
    except ServerSelectionTimeoutError:
        return False
    return True

def isRedisOnline():
    pool = redis.ConnectionPool(host=globalSettings.getRedisIP(), port=6379, db=0)
    r = redis.Redis(connection_pool=pool)
    try:
        r.ping()
    except ConnectionError:
        return False
    return True


def isNeo4jOnline():
    try:
        Graph(globalSettings.getNeo4jURI())
    except SocketError:
        return False
    return True

def isPostgreOnline():
    psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
    try:
        psycopg2.connect("dbname='postgres' user='postgres' host='"+globalSettings.getPostgreIP()+"' password=''")
    except OperationalError:
        return False
    return True



def insertErrorCodesIntoMongo(dictOfErrors, delete):
    # Datenbankzugriff
    db = globalSettings.getDatabaseItems()
    finalCollection = db.errorCodes

    if delete:
        finalCollection.remove({})
    brand = dictOfErrors['brand']
    errors = deepcopy(dictOfErrors['errors'])


    for singleError in errors:
        #print(singleError)
        singleError['brand'] = brand
        finalCollection.insert(singleError)

def insertErrorCodesIntoRedis(dictOfErrors, delete):
    pool = redis.ConnectionPool(host=globalSettings.getRedisIP(), port=6379, db=0)
    r = redis.Redis(connection_pool=pool)

    if delete:
        keys = r.keys('*')
        for key in keys :
            r.delete(key)

    errors = deepcopy(dictOfErrors['errors'])

    for singleError in errors:
        errorString = dictOfErrors["brand"] + "_" + singleError["code"]
        r.set(errorString, json.dumps(singleError))

def insertErrorCodesIntoNeo4J(dictOfErrors, delete):

    authenticate(globalSettings.getNeo4jIP()+":7474", 'neo4j', '123456')
    graph = Graph(globalSettings.getNeo4jURI())

    if delete:
        tx = graph.begin()
        graph.delete_all()
        tx.commit()

    tx = graph.begin()
    errorDict = deepcopy(dictOfErrors["errors"])
    brand = Node("Brand", name=dictOfErrors["brand"])
    tx.create(brand)

    errors = []

    for singleError in errorDict:
        errorNode = Node("Error", code=singleError["code"], explanation=singleError["explanation"], solution=singleError["solution"])
        errors.append(errorNode)
        tx.create(errorNode)
        #tx.append("CREATE (" + dictOfErrors["brand"] + "_" + singleError["code"] + ":Error {Solution:" + singleError["solution"] + "})")

    for error in errors:
        manufactor = Relationship(brand, "HAS", error)
        manufactor2 = Relationship(error, "IS MADE BY", brand)
        tx.create(manufactor)
        tx.create(manufactor2)

    tx.commit()

def insertErrorCodesIntoPostgre(dictOfErrors, delete):
    psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='"+globalSettings.getPostgreIP()+"' password=''")

    cur = conn.cursor()
    if delete:
        cur.execute("DROP TABLE IF EXISTS errors")
        cur.execute("""CREATE TABLE public.errors(
      brand VARCHAR,
      code VARCHAR NOT NULL,
      explanation VARCHAR,
      solution VARCHAR,
      CONSTRAINT errors_brand_code_pk PRIMARY KEY (brand, code));""")

    brand = dictOfErrors['brand']
    errors = deepcopy(dictOfErrors["errors"])
    for singleError in errors:
        singleError['brand'] = brand
        cur.execute("""INSERT INTO  errors (brand, code, explanation, solution) VALUES (%(brand)s, %(code)s, %(explanation)s, %(solution)s) ON CONFLICT DO NOTHING""",singleError)
    cur.execute("commit")

def insertContactDataIntoMongo(dictOfContacts, delete):

    client = MongoClient(globalSettings.getMongoIP(), 27017)
    db = client.items
    finalCollection = db.contactData

    if delete:
        finalCollection.remove({})

    organisation = dictOfContacts['organisation']
    contacts = deepcopy(dictOfContacts["contacts"])


    print(dictOfContacts)
    print(contacts)

    for singleContact in contacts:
        singleContact["Organisation"] = organisation

    finalCollection.insert(contacts)


def insertContactDataIntoRedis(dictOfContacts, delete):
    pool = redis.ConnectionPool(host=globalSettings.getRedisIP(), port=6379, db=1)
    r = redis.Redis(connection_pool=pool)

    if delete:
        keys = r.keys('*')
        for key in keys :
            r.delete(key)

    organisation = dictOfContacts['organisation']
    contacts = deepcopy(dictOfContacts['contacts'])

    r.set(organisation, json.dumps(contacts))


def insertContactDataIntoNeo4J(dictOfContacts, delete):

    authenticate(globalSettings.getNeo4jIP()+":7474", 'neo4j', '123456')
    graph = Graph("http://"+globalSettings.getNeo4jIP()+":7474")

    if delete:
        tx = graph.begin()
        graph.delete_all()
        tx.commit()

    tx = graph.begin()
    contactDict = deepcopy(dictOfContacts['contacts'])
    organisation = Node('Organisation', name=dictOfContacts['organisation'])
    tx.create(organisation)

    contacts = []

    for singleContact in contactDict:
        print(contactDict[singleContact]['Name'])
        contactName = contactDict[singleContact]['Name']
        if contactName == {}:
            contactName = "-"
        print(contactDict[singleContact]['E-Mail'])
        contactEmail = contactDict[singleContact]['E-Mail']
        if contactEmail == {}:
            contactEmail = "-"
        print(contactDict[singleContact]['Info/Adresse'])
        contactInfo = contactDict[singleContact]['E-Mail']
        if contactInfo == {}:
            contactInfo = "-"
        print(contactDict[singleContact]['Telefon'])
        contactTelefone = contactDict[singleContact]['E-Mail']
        if contactTelefone == {}:
            contactTelefone = "-"
        contactNode = Node('Contact', name=contactName, email=contactEmail, info_adress=contactInfo, telefone=contactTelefone)
        contacts.append(contactNode)
        tx.create(contactNode)

    for contact in contacts:
        manufactor = Relationship(organisation, "HAS", contact)
        manufactor2 = Relationship(contact, "IS PART OF", organisation)
        tx.create(manufactor)
        tx.create(manufactor2)

    tx.commit()

def insertContactDataIntoPostgre(dictOfContacts, delete):
    psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='"+globalSettings.getPostgreIP()+"' password=''")
    cur = conn.cursor()

    if delete:
        cur.execute("DROP TABLE IF EXISTS contacts")

    cur.execute("""CREATE TABLE public.contacts(
      organisation VARCHAR,
      name VARCHAR NOT NULL,
      email VARCHAR,
      info_address VARCHAR,
      telefone VARCHAR,
      CONSTRAINT contacts_organisation_name_pk PRIMARY KEY (organisation, name));""")

    organisation = dictOfContacts['organisation']
    contacts = deepcopy(dictOfContacts['contacts'])

    for singleContact in contacts:
        contacts[singleContact]['Organisation'] = organisation
        cur.execute("""INSERT INTO  contacts (organisation, name, email, info_address, telefone) VALUES (%(Organisation)s, %(Name)s, %(E-Mail)s, %(Info/Adresse)s, %(Telefon)s) ON CONFLICT DO NOTHING""",contacts[singleContact])
    cur.execute("commit")

def getErrorCodesFromMongo(listOfErrorCodes):
    # Datenbankzugriff
    db = globalSettings.getDatabaseItems()
    finalCollection = db.errorCodes

    listOfFullErrors = []

    print(listOfErrorCodes)

    for singleErrorCode in listOfErrorCodes:
        print(singleErrorCode)
        results = finalCollection.find({"$and"[{'code': {'$regex':singleErrorCode['code']}, 'brand': {'$regex':singleErrorCode['brand']}}]})
        for singleResult in results:
            listOfFullErrors.append(singleResult)

    return listOfFullErrors

def getErrorCodesFromRedis(listOfErrorCodes):
    pool = redis.ConnectionPool(host=globalSettings.getRedisIP(), port=6379, db=0)
    r = redis.Redis(connection_pool=pool)

    listOfFullErrors = []

    for singleErrorCode in listOfErrorCodes:
        listOfFullErrors.append(r.get(singleErrorCode))

    return listOfFullErrors

def getErrorCodesFromNeo4J(listOfErrorCodes):
    authenticate(globalSettings.getNeo4jIP() + ":7474", 'neo4j', '123456')
    graph = Graph("http://" + globalSettings.getNeo4jIP() + ":7474/db/data/")

    listOfFullErrors = []

    for singleErrorCode in listOfErrorCodes:
        listOfFullErrors.append(graph.find(singleErrorCode))

    return listOfFullErrors

def getErrorCodesFromPostgre(listOfErrorCodes):
    psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='"+globalSettings.getPostgreIP()+"' password=''")

    cur = conn.cursor()

    listOfFullErrors = []

    for singleErrorCode in listOfErrorCodes:
        cur.execute("""SELECT * FROM errors WHERE code = %s""", (singleErrorCode, ))
        listOfFullErrors.append(cur.fetchall())
        cur.execute("commit")

    return listOfFullErrors


def getContactDataFromMongo(listOfContacts):
    # Datenbankzugriff
    db = globalSettings.getDatabaseItems()
    finalCollection = db.contactData

    listOfFullContacts = []

    for singleContact in listOfContacts:
        results = finalCollection.find({"$and"[{'code': {'$regex':singleContact['organisation']}},
                                        {'name': {'$regex':singleContact['name']}},
                                        {'email': {'$regex':singleContact['email']}},
                                        {'phone': {'$regex':singleContact['phone']}},
                                        {'info': {'$regex':singleContact['info']}}]})
        for singleResult in results:
            listOfFullContacts.append(singleResult)

    return listOfFullContacts

def getContactDataFromRedis(listOfContacts):
    pool = redis.ConnectionPool(host=globalSettings.getRedisIP(), port=6379, db=0)
    r = redis.Redis(connection_pool=pool)

    listOfFullContacts = []

    for singleContact in listOfContacts:
        listOfFullContacts.append(r.get(singleContact))

    return listOfFullContacts

def getContactDataFromNeo4J(listOfContacts):
    authenticate(globalSettings.getNeo4jIP() + ":7474", 'neo4j', '123456')
    graph = Graph("http://" + globalSettings.getNeo4jIP() + ":7474/db/data/")

    listOfFullContacts = []

    for singleContact in listOfContacts:
        listOfFullContacts.append(graph.find(singleContact))

    return listOfFullContacts

def getContactDataFromPostgre(listOfContacts):
    psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='"+globalSettings.getPostgreIP()+"' password=''")

    cur = conn.cursor()

    listOfFullContacts = []

    for singleContact in listOfContacts:
        cur.execute("""SELECT * FROM errors WHERE organisation = %s""", (singleContact, ))
        listOfFullContacts.append(cur.fetchall())
        cur.execute("commit")

    return listOfFullContacts

def getMongoSizeOfCollection(collection):
    return globalSettings.getDatabaseItems().command("collstats", collection)['storageSize']

def getRedisSize():
    return 0
    #TODO: implement

def getPostgreSize():
    psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
    conn = psycopg2.connect("dbname='postgres' user='postgres' host='"+globalSettings.getPostgreIP()+"' password=''")
    cur = conn.cursor()
    cur.execute("""SELECT * FROM pg_total_relation_size('errors')""")
    cur.fetchall()

    return 0
