import random
import time
import numpy

import itertools
from copy import deepcopy


import postprocessing

from scrapy.crawler import CrawlerProcess, CrawlerRunner
from crawler.printerCrawler.spiders.printer import Brother
from twisted.internet import reactor
from scrapy.settings import Settings
from collections import defaultdict
import globalSettings

import os
from sys import path
from os.path import dirname
path.append(dirname(dirname(os.path.realpath(__file__)) + '\\crawler\\'))

# Settings for generic site
import databases

class Benchmarking:

    # Variablen fuer Crawler & Postprocessing
    brand = None
    url = None
    area = None
    codeElement = None
    explanationElement = None
    solutionElement = None
    errorDict = None

    # brand = canon, brother oder printertechs
    @staticmethod
    def generateTestData(brand, toCrawl):
        Benchmarking.brand = brand

        # Domains fuer Scrappy
        mydomains = [
                    "brotherprinterrepairs.co.uk"
                    # "dasendedesinternet.de",
                    # "support.hp.com"
                    # "http://www.canon-europe.com/printers/home-printers"
                ]

        # URLs fuer Scrappy
        myurls = [
                    "http://www.brotherprinterrepairs.co.uk/common-errors"
                    # "http://www.dasendedesinternet.de",
                    # "http://support.hp.com/si-en/document/c01458034",
                    # "http://www.canon-europe.com/printers/home-printers"
                ]

        Benchmarking.area = defaultdict(list)
        Benchmarking.codeElement = defaultdict(list)
        Benchmarking.explanationElement = defaultdict(list)
        Benchmarking.solutionElement = defaultdict(list)

        # Check welche Seite
        if(brand == "canon"):
            Benchmarking.url = "http://www.dfelten.de/canon/test.html"
            # area
            Benchmarking.area['elem'] = "div"
            Benchmarking.area['class'] = "c-full-section-listing"
            # code
            Benchmarking.codeElement['elem'] = "h2"
            Benchmarking.codeElement['class'] = "c-denda"
            # explanation
            Benchmarking.explanationElement['elem'] = "p"
            Benchmarking.explanationElement['class'] = "c-detail-introtext"
            # solution
            Benchmarking.solutionElement['elem'] = "div"
            Benchmarking.solutionElement['class'] = "c-detail-listing-img"

        if(brand == "printertechs"):
            Benchmarking.url = "http://www.printertechs.com/printer-troubleshooting/369-50-1-error-on-hp-laserjet-5000-5100"
            # area
            Benchmarking.area['elem'] = "div"
            Benchmarking.area['class'] = "item-page"
            # code
            Benchmarking.codeElement['elem'] = "h1"
            Benchmarking.codeElement['class'] = "bottomline"
            # explanation
            Benchmarking.explanationElement['elem'] = "p"
            Benchmarking.explanationElement['class'] = ""

        if (brand == "brother"):
            Benchmarking.url = "http://www.brotherprinterrepairs.co.uk/common-errors/"
            # area
            Benchmarking.area['elem'] = "div"
            Benchmarking.area['class'] = "error_codes"
            # code
            Benchmarking.codeElement['elem'] = "p"
            Benchmarking.codeElement['class'] = "error_codes_content_sml"

        # list of crawlers
        TO_CRAWL = [Brother]

        # crawlers that are running
        RUNNING_CRAWLERS = []

        if toCrawl:
            # Datenbank leeren
            # items = globalSettings.getDatabaseItems()
            # items.printercollection.remove({})


            # Webcrawler starten
            # set up the crawler and start to crawl one spider at a time
            for spider in TO_CRAWL:
                print mydomains
                print myurls
                settings = Settings()
                settings.set('BOT_NAME', 'brother')
                settings.set('SPIDER_MODULES', ['crawler.printerCrawler.spiders'])
                settings.set('NEWSPIDER_MODULE', 'crawler.printerCrawler.spiders')
                settings.set('LOG_LEVEL', 'INFO')
                settings.set('ITEM_PIPELINES', {'printerCrawler.Pipeline.MongoPipeline.MongoPipeline': 100})
                settings.set('MONGODB_SERVER', "127.0.0.1")
                # settings.set('MONGODB_SERVER', "192.168.99.100")
                settings.set('MONGODB_PORT', 27017)
                settings.set('MONGODB_DB', '"items"')
                settings.set('MONGODB_COLLECTION', '"printercollection"')
                settings.set('DOWNLOAD_HANDLERS', {'s3': None})

                print settings
                runner = CrawlerRunner(settings)
                crawler_obj = spider()
                Brother.allowed_domains = mydomains
                Brother.start_urls = myurls
                RUNNING_CRAWLERS.append(crawler_obj)
                d = runner.crawl(crawler_obj)
                try:
                    reactor.stop(0)
                except:
                    print("Reactor not running")
                # d.addBoth(lambda _: reactor.stop())
                print("Vor dem run")
                reactor.run(0)
                print("Nach dem run")

                #crawler.start(True)

    def printTheShit(x):
        print("Deffered ist fertig")
        print(x)

    @staticmethod
    def prepareErrorDict():
        # if(Benchmarking.brand == None):
        #     Benchmarking.brand = "brother"
        # if (Benchmarking.url == None):
        #     Benchmarking.url = "brotherprinterrepairs.co.uk"
        # if(Benchmarking.area == None):
        #     Benchmarking.area = "brother"
        # if(Benchmarking.codeElement == None):
        #     Benchmarking.codeElement = "brother"
        # if(Benchmarking.explanationElement == None):
        #     Benchmarking.explanationElement = "brother"
        # if(Benchmarking.solutionElement == None):
        #     Benchmarking.solutionElement = "brother"
        # print(Benchmarking.brand)
        # print(Benchmarking.url)
        # print(Benchmarking.area)
        # print(Benchmarking.codeElement)
        # print(Benchmarking.explanationElement)
        # print(Benchmarking.solutionElement)
        Benchmarking.errorDict = postprocessing.GetErrorCodesGeneric(Benchmarking.brand, Benchmarking.url, Benchmarking.area, Benchmarking.codeElement, Benchmarking.explanationElement, Benchmarking.solutionElement)

        # print(Benchmarking.errorDict)

    # Soll mit deepcopy gearbeitet werden?
    useDeepcopy = False

    @staticmethod
    def benchmark(datenbanken, obergrenze, leseObergrenze, wiederholungen, operationen):
        insertTimes = defaultdict(list)
        deleteTimes = defaultdict(list)
        readTimes = defaultdict(list)
        errorDicts = defaultdict(dict)

        print("Original amount of data: " + str(len(Benchmarking.errorDict["errors"])))
        for index in xrange(1, obergrenze+1):
            print("Doing copy for multiplier " + str(index))
            newDict = defaultdict(list)
            newDict["brand"] = deepcopy(Benchmarking.errorDict["brand"])
            for error in Benchmarking.errorDict["errors"]:  # TODO: wahrscheinlich geht das performanter und am besten noch abhaengig vom Paramter "potences"
                for i in xrange(index):
                    newError = defaultdict(list)
                    if(Benchmarking.useDeepcopy):
                        newError["code"] = deepcopy(error["code"] + str(i))
                        newError["description"] = deepcopy(error["description"])
                        newError["solution"] = deepcopy(error["solution"])
                        newError["explanation"] = deepcopy(error["explanation"])
                    else:
                        newError["code"] = error["code"] + str(i)
                        newError["description"] = error["description"]
                        newError["solution"] = error["solution"]
                        newError["explanation"] = error["explanation"]
                    newDict["errors"].append(newError)
            errorDicts[index] = newDict
            print("Amount of Data in set " + str(index) + ": " + str(len(newDict["errors"])))

        delete = True;
        currentNo = 1;
        totalNo = str(len(datenbanken) * obergrenze * wiederholungen)

        if 'mongo' in datenbanken:
            for r in xrange(wiederholungen):  # Bessere normale For-Schleife, die 32% effizienter ist als die normale Syntax
                for i in xrange(1, obergrenze+1):
                    if 'einfuegen' in operationen:
                        if not delete:
                            databases.insertErrorCodesIntoMongo(defaultdict(list), True)
                        startingTime = time.time()
                        databases.insertErrorCodesIntoMongo(errorDicts[int(i)], delete)
                        fininishingTime = time.time()
                        seconds = fininishingTime - startingTime
                        milis = int(round(seconds * 1000))
                        insertTimes["Mongo" + str(i)].append(milis)
                    if 'loeschen' in operationen:
                        databases.insertErrorCodesIntoMongo(errorDicts[int(i)], True)
                        startingTime = time.time()
                        databases.insertErrorCodesIntoMongo(defaultdict(list), True)
                        fininishingTime = time.time()
                        seconds = fininishingTime - startingTime
                        milis = int(round(seconds * 1000))
                        deleteTimes["Mongo" + str(i)].append(milis)
                    if 'lesen' in operationen:
                        databases.insertErrorCodesIntoMongo(errorDicts[int(i)], True)
                        random.seed()
                        errorCodes = []
                        for j in xrange(1,leseObergrenze+1):
                            rn = random.randint(1, 101 * i) - 1
                            errorCodes.append(errorDicts[int(i)]['errors'][rn]['code'])
                            startingTime = time.time()
                            databases.getErrorCodesFromMongo(errorCodes)
                            fininishingTime = time.time()
                            seconds = fininishingTime - startingTime
                            milis = int(round(seconds * 1000))
                            readTimes["Mongo" + str(i) + "_" + str(j)].append(milis)

                    # print("Inserting error codes into MongoDB" + str(i) + " took " + str(seconds) + "seconds = " + str(milis) + " miliseconds")
                    print("Finished " + str(currentNo) + " of " + totalNo)
                    currentNo = currentNo + 1


        if 'redis' in datenbanken:
            for r in itertools.repeat(None, wiederholungen):  # Bessere normale For-Schleife, die 32% effizienter ist als die normale Syntax
                for i in xrange(1, obergrenze+1):
                    if 'einfuegen' in operationen:
                        if not delete:
                            databases.insertErrorCodesIntoRedis(defaultdict(list), True)
                        startingTime = time.time()
                        databases.insertErrorCodesIntoRedis(errorDicts[int(i)], delete)
                        fininishingTime = time.time()
                        seconds = fininishingTime - startingTime
                        milis = int(round(seconds * 1000))
                        insertTimes["Redis" + str(i)].append(milis)
                    if 'loeschen' in operationen:
                        databases.insertErrorCodesIntoRedis(errorDicts[int(i)], True)
                        startingTime = time.time()
                        databases.insertErrorCodesIntoRedis(defaultdict(list), True)
                        fininishingTime = time.time()
                        seconds = fininishingTime - startingTime
                        milis = int(round(seconds * 1000))
                        deleteTimes["Redis" + str(i)].append(milis)
                    if 'lesen' in operationen:
                        databases.insertErrorCodesIntoRedis(errorDicts[int(i)], True)
                        random.seed()
                        errorCodes = []
                        for j in xrange(1,leseObergrenze+1):
                            rn = random.randint(1, 101 * i) - 1
                            errorCodes.append(errorDicts[int(i)]['errors'][rn]['code'])
                            startingTime = time.time()
                            databases.getErrorCodesFromRedis(errorCodes)
                            fininishingTime = time.time()
                            seconds = fininishingTime - startingTime
                            milis = int(round(seconds * 1000))
                            readTimes["Redis" + str(i) + "_" + str(j)].append(milis)

                    # print("Inserting error codes into RedisDB" + str(i) + " took " + str(seconds) + "seconds = " + str(milis) + " miliseconds")
                    print("Finished " + str(currentNo) + " of " + totalNo)
                    currentNo = currentNo + 1

        if 'postgres' in datenbanken:
            for r in itertools.repeat(None, wiederholungen):  # Bessere normale For-Schleife, die 32% effizienter ist als die normale Syntax
                for i in xrange(1, obergrenze+1):
                    if 'einfuegen' in operationen:
                        if not delete:
                            databases.insertErrorCodesIntoPostgre(defaultdict(list), True)
                        startingTime = time.time()
                        databases.insertErrorCodesIntoPostgre(errorDicts[int(i)], delete)
                        fininishingTime = time.time()
                        seconds = fininishingTime - startingTime
                        milis = int(round(seconds * 1000))
                        insertTimes["Postgre" + str(i)].append(milis)
                    if 'loeschen' in operationen:
                        databases.insertErrorCodesIntoPostgre(errorDicts[int(i)], True)
                        startingTime = time.time()
                        databases.insertErrorCodesIntoPostgre(defaultdict(list), True)
                        fininishingTime = time.time()
                        seconds = fininishingTime - startingTime
                        milis = int(round(seconds * 1000))
                        deleteTimes["Postgre" + str(i)].append(milis)
                    if 'lesen' in operationen:
                        databases.insertErrorCodesIntoPostgre(errorDicts[int(i)], True)
                        random.seed()
                        errorCodes = []
                        for j in xrange(1,leseObergrenze+1):
                            rn = random.randint(1, 101 * i) - 1
                            errorCodes.append(errorDicts[int(i)]['errors'][rn]['code'])
                            startingTime = time.time()
                            databases.getErrorCodesFromPostgre(errorCodes)
                            fininishingTime = time.time()
                            seconds = fininishingTime - startingTime
                            milis = int(round(seconds * 1000))
                            readTimes["Postgre" + str(i) + "_" + str(j)].append(milis)

                    # print("Inserting error codes into PostgreDB" + str(i) + " took " + str(seconds) + "seconds = " + str(milis) + " miliseconds")
                    print("Finished " + str(currentNo) + " of " + totalNo)
                    currentNo = currentNo + 1
        # for _ in itertools.repeat(None, repetitions):  # Bessere normale For-Schleife, die 32% effizienter ist als die normale Syntax
        #     startingTime = time.time()
        #     databases.insertErrorCodesIntoNeo4J(errorDict)
        #     fininishingTime = time.time()
        #     seconds = fininishingTime - startingTime
        #     milis = int(round(seconds * 1000))
        #     times["Neo4j"].append(milis)
        #     print("Inserting error codes into Neo4J took " + str(seconds) + "seconds = " + str(milis) + " miliseconds")

        print("Finished with benchmarking Databases! Results: ")
        for index in xrange(1, obergrenze+1):
            if 'einfuegen' in operationen:
                print("Mongo" + str(index) + ": " + str(insertTimes["Mongo" + str(index)]))
                mean = numpy.mean(insertTimes["Mongo" + str(index)], axis = 0)
                std = numpy.std(insertTimes["Mongo" + str(index)], axis = 0)
                print("Mongo" + str(index) + " mean: " + str(mean) + ", standard deviation: " + str(std))
                insertTimes["Mongo" + str(index) + "mean"] = mean
                insertTimes["Mongo" + str(index) + "std"] = std
                print("Redis" + str(index) + ": " + str(insertTimes["Redis" + str(index)]))
                mean = numpy.mean(insertTimes["Redis" + str(index)], axis = 0)
                std = numpy.std(insertTimes["Redis" + str(index)], axis = 0)
                print("Redis" + str(index) + " mean: " + str(mean) + ", standard deviation: " + str(std))
                insertTimes["Redis" + str(index) + "mean"] = mean
                insertTimes["Redis" + str(index) + "std"] = std
                print("Postgre" + str(index) + ": " + str(insertTimes["Postgre" + str(index)]))
                mean = numpy.mean(insertTimes["Postgre" + str(index)], axis = 0)
                std = numpy.std(insertTimes["Postgre" + str(index)], axis = 0)
                print("Postgre" + str(index) + " mean: " + str(mean) + ", standard deviation: " + str(std))
                insertTimes["Postgre" + str(index) + "mean"] = mean
                insertTimes["Postgre" + str(index) + "std"] = std
            if 'loeschen' in operationen:
                print("Mongo" + str(index) + ": " + str(deleteTimes["Mongo" + str(index)]))
                mean = numpy.mean(deleteTimes["Mongo" + str(index)], axis = 0)
                std = numpy.std(deleteTimes["Mongo" + str(index)], axis = 0)
                print("Mongo" + str(index) + " mean: " + str(mean) + ", standard deviation: " + str(std))
                deleteTimes["Mongo" + str(index) + "mean"] = mean
                deleteTimes["Mongo" + str(index) + "std"] = std
                print("Redis" + str(index) + ": " + str(deleteTimes["Redis" + str(index)]))
                mean = numpy.mean(deleteTimes["Redis" + str(index)], axis = 0)
                std = numpy.std(deleteTimes["Redis" + str(index)], axis = 0)
                print("Redis" + str(index) + " mean: " + str(mean) + ", standard deviation: " + str(std))
                deleteTimes["Redis" + str(index) + "mean"] = mean
                deleteTimes["Redis" + str(index) + "std"] = std
                print("Postgre" + str(index) + ": " + str(deleteTimes["Postgre" + str(index)]))
                mean = numpy.mean(deleteTimes["Postgre" + str(index)], axis = 0)
                std = numpy.std(deleteTimes["Postgre" + str(index)], axis = 0)
                print("Postgre" + str(index) + " mean: " + str(mean) + ", standard deviation: " + str(std))
                deleteTimes["Postgre" + str(index) + "mean"] = mean
                deleteTimes["Postgre" + str(index) + "std"] = std
            if 'lesen' in operationen:
                for i in xrange(1, 101):
                    print("Mongo" + str(index) + " with " + str(i) + " reads: " + str(readTimes["Mongo" + str(index) + "_" + str(i)]))
                    mean = numpy.mean(readTimes["Mongo" + str(index) + "_" + str(i)], axis = 0)
                    std = numpy.std(readTimes["Mongo" + str(index) + "_" + str(i)], axis = 0)
                    print("Mongo" + str(index) + " with " + str(i) + " reads mean: " + str(mean) + ", standard deviation: " + str(std))
                    readTimes["Mongo" + str(index) + "_" + str(i) + "mean"] = mean
                    readTimes["Mongo" + str(index) + "_" + str(i) + "std"] = std
                    print("Redis" + str(index) + " with " + str(i) + " reads: " + str(readTimes["Redis" + str(index) + "_" + str(i)]))
                    mean = numpy.mean(readTimes["Redis" + str(index) + "_" + str(i)], axis = 0)
                    std = numpy.std(readTimes["Redis" + str(index) + "_" + str(i)], axis = 0)
                    print("Redis" + str(index) + " mean: " + str(mean) + ", standard deviation: " + str(std))
                    readTimes["Redis" + str(index) + "_" + str(i) + "mean"] = mean
                    readTimes["Redis" + str(index) + "_" + str(i) + "std"] = std
                    print("Postgre" + str(index) + " with " + str(i) + " reads: " + str(readTimes["Postgre" + str(index) + "_" + str(i)]))
                    mean = numpy.mean(readTimes["Postgre" + str(index) + "_" + str(i)], axis = 0)
                    std = numpy.std(readTimes["Postgre" + str(index) + "_" + str(i)], axis = 0)
                    print("Postgre" + str(index) + " mean: " + str(mean) + ", standard deviation: " + str(std))
                    readTimes["Postgre" + str(index) + "_" + str(i) + "mean"] = mean
                    readTimes["Postgre" + str(index) + "_" + str(i) + "std"] = std
                    # print("Mongo" + str(index) + " with " + str(i) + " reads: " + str(readTimes["Mongo" + str(index)][0][str(i)]))
                    # mean = numpy.mean(readTimes["Mongo" + str(index)][0][str(i)], axis = 0)
                    # std = numpy.std(readTimes["Mongo" + str(index)][0][str(i)], axis = 0)
                    # print("Mongo" + str(index) + " with " + str(i) + " reads mean: " + str(mean) + ", standard deviation: " + str(std))
                    # print("Redis" + str(index) + " with " + str(i) + " reads: " + str(readTimes["Redis" + str(index)][0][str(i)]))
                    # mean = numpy.mean(readTimes["Redis" + str(index)][0][str(i)], axis = 0)
                    # std = numpy.std(readTimes["Redis" + str(index)][0][str(i)], axis = 0)
                    # print("Redis" + str(index) + " mean: " + str(mean) + ", standard deviation: " + str(std))
                    # print("Postgre" + str(index) + " with " + str(i) + " reads: " + str(readTimes["Postgre" + str(index)][0][str(i)]))
                    # mean = numpy.mean(readTimes["Postgre" + str(index)][0][str(i)], axis = 0)
                    # std = numpy.std(readTimes["Postgre" + str(index)][0][str(i)], axis = 0)
                    # print("Postgre" + str(index) + " mean: " + str(mean) + ", standard deviation: " + str(std))

        results = defaultdict(list)

        if 'einfuegen' in operationen:
            results["einfuegen"] = insertTimes
        if 'lesen' in operationen:
            results["lesen"] = readTimes
        if 'loeschen' in operationen:
            results["loeschen"] = deleteTimes
        print(results)
        # print(results["lesen"])
        # print(results["lesen"]["Mongo1_1"])
        return results

    @staticmethod
    def dummyBenchmarkData():
        result = defaultdict(list)
        test = {'Postgre10std': 62.497999967998979, 'Mongo12std': 60.538325785314619, 'Mongo10mean': 549.33333333333337,
         'Mongo4mean': 326.0, 'Postgre9mean': 392.66666666666669, 'Redis16mean': 585.0,
         'Postgre20std': 121.44225879907793, 'Redis6std': 24.239545283597124, 'Postgre3std': 20.992061991778385,
         'Postgre18mean': 645.33333333333337, 'Redis15std': 20.542638584174139, 'Mongo13std': 23.753362335093154,
         'Mongo7std': 61.584630117153957, 'Redis19mean': 703.66666666666663, 'Redis12std': 78.112454548270051,
         'Mongo17std': 24.239545283597124, 'Redis15mean': 553.0, 'Redis13std': 25.223445883190152, 'Mongo7mean': 439.0,
         'Postgre15std': 28.767265347188555, 'Postgre8std': 38.738439135652676, 'Mongo6std': 20.368821489936252,
         'Postgre17mean': 499.66666666666669, 'Mongo16mean': 928.33333333333337, 'Mongo12mean': 758.33333333333337,
         'Postgre11mean': 392.66666666666669, 'Mongo18std': 157.75367578038308, 'Mongo5mean': 338.33333333333331,
         'Mongo6mean': 379.66666666666669, 'Mongo14mean': 790.33333333333337, 'Redis13mean': 535.66666666666663,
         'Postgre10mean': 417.0, 'Postgre5std': 23.338094752285727, 'Postgre19std': 24.166091947189145,
         'Redis2mean': 160.0, 'Postgre7mean': 304.66666666666669, 'Redis9mean': 362.66666666666669,
         'Mongo4std': 64.379085630868246, 'Redis2std': 3.2659863237109041, 'Postgre19mean': 585.0,
         'Postgre7std': 22.305953365762146, 'Postgre14': [445, 420, 476], 'Postgre15': [528, 466, 468],
         'Postgre16': [447, 466, 516], 'Postgre17': [487, 520, 492], 'Postgre10': [342, 495, 414],
         'Postgre11': [378, 383, 417], 'Postgre12': [376, 392, 445], 'Postgre13': [444, 487, 398],
         'Postgre18std': 41.907305117631012, 'Redis17mean': 683.33333333333337, 'Postgre18': [687, 588, 661],
         'Postgre19': [565, 619, 571], 'Redis9std': 5.7927157323275891, 'Redis20': [682, 701, 743],
         'Mongo3std': 17.048949136725895, 'Redis1std': 7.7888809636986149, 'Redis7mean': 316.33333333333331,
         'Postgre4std': 29.97776954122282, 'Postgre12mean': 404.33333333333331, 'Postgre6': [315, 260, 381],
         'Postgre7': [282, 335, 297], 'Postgre4': [327, 269, 259], 'Postgre5': [241, 227, 282],
         'Postgre2': [244, 178, 183], 'Postgre3': [230, 219, 181], 'Postgre1': [197, 275, 250],
         'Redis5': [263, 295, 283], 'Redis4': [220, 222, 230], 'Redis7': [326, 327, 296], 'Redis6': [329, 271, 289],
         'Redis1': [341, 360, 349], 'Postgre8': [289, 305, 378], 'Postgre9': [406, 444, 328], 'Postgre3mean': 210.0,
         'Postgre2mean': 201.66666666666666, 'Redis6mean': 296.33333333333331, 'Redis19std': 49.815214097257034,
         'Redis19': [764, 705, 642], 'Redis18': [618, 637, 681], 'Mongo11mean': 669.33333333333337,
         'Redis20mean': 708.66666666666663, 'Redis11': [427, 488, 428], 'Redis10': [390, 402, 449],
         'Redis13': [530, 569, 508], 'Redis12': [474, 627, 451], 'Redis15': [537, 540, 582], 'Redis14': [522, 521, 536],
         'Redis17': [604, 681, 765], 'Redis16': [570, 566, 619], 'Mongo6': [408, 370, 361], 'Mongo7': [399, 526, 392],
         'Mongo4': [417, 278, 283], 'Mongo5': [321, 370, 324], 'Mongo2': [231, 194, 265], 'Mongo3': [276, 242, 238],
         'Mongo1': [187, 160, 195], 'Mongo3mean': 252.0, 'Postgre14mean': 447.0, 'Mongo8': [754, 665, 734],
         'Mongo9': [488, 569, 574], 'Postgre13mean': 443.0, 'Mongo8std': 38.125523675820581, 'Postgre5mean': 250.0,
         'Postgre1mean': 240.66666666666666, 'Postgre14std': 22.90560339014597, 'Mongo15std': 27.812866726670869,
         'Redis4mean': 224.0, 'Mongo13mean': 694.66666666666663, 'Mongo1std': 14.974051630144134,
         'Mongo14std': 123.22427610751959, 'Postgre15mean': 487.33333333333331, 'Mongo14': [716, 964, 691],
         'Mongo15': [844, 785, 785], 'Mongo16': [1054, 793, 938], 'Mongo17': [914, 874, 856],
         'Mongo10': [571, 524, 553], 'Mongo11': [755, 653, 600], 'Mongo12': [843, 727, 705], 'Mongo13': [725, 692, 667],
         'Mongo18': [926, 950, 1272], 'Mongo19': [963, 991, 921], 'Redis11mean': 447.66666666666669,
         'Postgre6std': 49.466037731850818, 'Redis3std': 21.483844059096022, 'Mongo2mean': 230.0,
         'Redis9': [355, 364, 369], 'Postgre9std': 48.28618389928485, 'Redis8': [341, 363, 344],
         'Redis11std': 28.522895287041877, 'Redis3mean': 216.66666666666666, 'Mongo20std': 123.62398185177862,
         'Mongo16std': 106.77182316614352, 'Postgre6mean': 318.66666666666669, 'Redis20std': 25.48637980482037,
         'Postgre16mean': 476.33333333333331, 'Redis14std': 6.8475461947247123, 'Postgre8mean': 324.0,
         'Redis1mean': 350.0, 'Postgre4mean': 285.0, 'Mongo20': [1239, 955, 1006], 'Mongo18mean': 1049.3333333333333,
         'Mongo9mean': 543.66666666666663, 'Postgre11std': 17.326921891156037, 'Redis10std': 25.460208605237749,
         'Redis3': [203, 200, 247], 'Redis5mean': 280.33333333333331, 'Mongo8mean': 717.66666666666663,
         'Redis14mean': 526.33333333333337, 'Redis2': [164, 160, 156], 'Postgre2std': 30.003703475108239,
         'Mongo5std': 22.425184255405547, 'Redis7std': 14.383632673594278, 'Mongo10std': 19.362047641943473,
         'Mongo17mean': 881.33333333333337, 'Redis8mean': 349.33333333333331, 'Postgre12std': 29.48822740612863,
         'Mongo11std': 64.323833495490263, 'Mongo1mean': 180.66666666666666, 'Redis18mean': 645.33333333333337,
         'Redis10mean': 413.66666666666669, 'Mongo19std': 28.767265347188555, 'Mongo9std': 39.415169104743867,
         'Redis17std': 65.748679750158402, 'Postgre16std': 29.101355447622861, 'Postgre17std': 14.522013940527977,
         'Mongo20mean': 1066.6666666666667, 'Redis8std': 9.7410927974683048, 'Postgre1std': 32.520079267362739,
         'Redis4std': 4.3204937989385739, 'Mongo19mean': 958.33333333333337, 'Mongo2std': 28.994252303976847,
         'Mongo15mean': 804.66666666666663, 'Postgre13std': 36.340977789083588, 'Postgre20mean': 638.33333333333337,
         'Redis18std': 26.386023236217735, 'Redis5std': 13.199326582148887, 'Redis16std': 24.097026095903757,
         'Redis12mean': 517.33333333333337, 'Postgre20': [810, 548, 557]}
        einfuegen = defaultdict(list)
        for k in test:
            einfuegen[k] = test[k]
        result["einfuegen"] = einfuegen;
        return result

# Benchmarking.generateTestData('brother', False)
# Benchmarking.prepareErrorDict()
# Benchmarking.benchmark(['mongo'], 1, 1, 1, ['einfuegen'])
# Benchmarking.benchmark(['mongo', 'redis', 'postgres'], 20, 100, 3, ['einfuegen'])
# result = Benchmarking.dummyBenchmarkData()
# print(result)
