import logging
import pprint
from scrapy.crawler import CrawlerRunner
from crawler.printerCrawler.spiders.printer import Brother
from scrapy.exceptions import CloseSpider
from twisted.internet import reactor
from scrapy.settings import Settings
import postprocessing
import brandSchemas
import globalSettings, databases

import os
from sys import path
from os.path import dirname
path.append(dirname(dirname(os.path.realpath(__file__)) + '\\crawler\\'))

# list of crawlers
TO_CRAWL = [Brother]
# crawlers that are running
RUNNING_CRAWLERS = []

# set up the crawler and start to crawl one spider at a time
def spiderCrawl(domain, url):
    print ("CRALWIECRAWLIE")
    for spider in TO_CRAWL:
        print ("spiderspider")
        settings = Settings()
        settings.set('BOT_NAME', 'brother')
        settings.set('SPIDER_MODULES', ['crawler.printerCrawler.spiders'])
        settings.set('NEWSPIDER_MODULE', 'crawler.printerCrawler.spiders')
        settings.set('LOG_LEVEL', 'INFO')
        settings.set('ITEM_PIPELINES', {'printerCrawler.Pipeline.MongoPipeline.MongoPipeline': 100})
        settings.set('MONGODB_SERVER', "127.0.0.1")
        # settings.set('MONGODB_SERVER', "192.168.99.100")
        settings.set('MONGODB_PORT', 27017)
        settings.set('MONGODB_DB', '"items"')
        settings.set('MONGODB_COLLECTION', '"printercollection"')
        settings.set('DOWNLOAD_HANDLERS',  {'s3': None})

        pprint.pprint(domain)
        pprint.pprint(url)

        print settings
        runner = CrawlerRunner(settings)
        crawler_obj = spider()
        Brother.allowed_domains = domain
        Brother.start_urls = url
        RUNNING_CRAWLERS.append(crawler_obj)
        d = runner.crawl(crawler_obj)
        d.addBoth(lambda _: reactor.stop())
        reactor.run(0)


# def spider_closing(spider):
#     # logging.msg("Spider closed: %s" % spider, level=logging.INFO)
#     RUNNING_CRAWLERS.remove(spider)
#     if not RUNNING_CRAWLERS:
#         reactor.stop()
def spider_closing():
    spider = RUNNING_CRAWLERS[0]
    # logging.msg("Spider closed: %s" % spider, level=logging.INFO)
    RUNNING_CRAWLERS.remove(spider)
    if not RUNNING_CRAWLERS:
        reactor.fireSystemEvent('shutdown')
# def spider_closing():
#     raise CloseSpider('bandwidth_exceeded')


# Datenbank leeren
def removeDatabaseEntries(clearDatabase):
    if clearDatabase:
        items = globalSettings.GetDatabase()
        items.printercollection.remove({})

# Funktion zum Start des Prozesses fuer Error Codes
def startErrorCodeProcess(insertIntoDatabases, startCrawler, crawlerDomains, crawlerURLs, deleteOldEntries,
                          errorBrand, errorUrl, errorArea, errorCodeElement, errorExplanationElement,
                          errorSolutionElement):
    # Webcrawler starten?
    if startCrawler:
        spiderCrawl(crawlerDomains, crawlerURLs)

    # Holen der Fehlercodes mit anschliessender Ausgabe anhand eines Schemas, sofern vorhanden
    if brandSchemas.existSchema(errorBrand, "error"):
        errorDict = brandSchemas.doPostprocessing(errorBrand, "error")
    else:
        errorDict = postprocessing.GetErrorCodesGeneric(errorBrand, errorUrl, errorArea, errorCodeElement,
                                                        errorExplanationElement, errorSolutionElement)

    # Testausgabe des Dict
    pprint.pprint(errorDict)

    # Eintragen der Error Codes in Datenbanken, sofern diese online sind
    if insertIntoDatabases:
        if databases.isMongoOnline:
            databases.insertErrorCodesIntoMongo(errorDict, deleteOldEntries)
        # if databases.isRedisOnline:
        #     databases.insertErrorCodesIntoRedis(errorDict, deleteOldEntries)
        # if databases.isPostgreOnline:
        #     databases.insertErrorCodesIntoPostgre(errorDict, deleteOldEntries)
        # if databases.isNeo4jOnline:
        #     databases.insertErrorCodesIntoNeo4J(errorDict, deleteOldEntries)

    return True

# Funktion zum Start des Prozesses fuer Kontaktdaten
def startContactProcess(insertIntoDatabases, startCrawler, crawlerDomains, crawlerURLs, deleteOldEntries,
                        contactOrganisation, contactUrl, contactArea, contactNameElement, contactInfoElement,
                        contactPhoneElement, contactEmailElement):
    # Webcrawler starten?
    if startCrawler:
        spiderCrawl(crawlerDomains, crawlerURLs)

    # Holen der Kontaktdaten mit anschliessender Ausgabe anhand eines Schemas, sofern vorhanden
    if brandSchemas.existSchema(contactOrganisation, "contact"):
        contactDict = brandSchemas.doPostprocessing(contactOrganisation, "contact")
    else:
        contactDict = postprocessing.GetContactDataGeneric(contactOrganisation, contactUrl, contactArea,
                                                           contactNameElement, contactInfoElement, contactPhoneElement,
                                                           contactEmailElement)
    # Testausgabe des Dict
    pprint.pprint(contactDict)

    # Eintragen der Kontaktdaten in Datenbanken, sofern diese online sind
    if insertIntoDatabases:
        if databases.isMongoOnline:
            databases.insertContactDataIntoMongo(contactDict, deleteOldEntries)
        # if databases.isRedisOnline:
        #     databases.insertContactDataIntoRedis(contactDict, deleteOldEntries)
        # if databases.isPostgreOnline:
        #     databases.insertContactDataIntoPostgre(contactDict, deleteOldEntries)
        # if databases.isNeo4jOnline:
        #     databases.insertContactDataIntoNeo4J(contactDict, deleteOldEntries)

    return True

def getRunning():
    return RUNNING_CRAWLERS[0]