# coding=utf-8
from pymongo import MongoClient
from bs4 import BeautifulSoup
from collections import defaultdict
from scripts import globalSettings
import pprint

# Error-Codes anhand eines Bereiches und Klassen der HTML-Elemente ermitteln
def GetErrorCodes(brand, url, area, codeElement, explanationElement, solutionElement):
    # Datenbankzugriff
    collection = globalSettings.getMongoErrorCodeCollection()
    errorList = defaultdict(list)

    # get all entrys from the database with the given url
    entries = collection.find({"url": url})
    for entry in entries:
        soup = BeautifulSoup(entry["body"], 'html.parser')

        # get all areas with error codes
        errorCodesClasses = soup.findAll(area['elem'], {"class": area['class']})
        for errorCodesClass in errorCodesClasses:
            tempList = defaultdict(list)

            if(not explanationElement):
                errorCodeID = errorCodesClass.find(codeElement['elem'], {"class": codeElement['class']}).string
                errorCodeID = str(errorCodeID).replace(".", ",")
                errorContent = errorCodesClass.findAll(codeElement['elem'], {"class": codeElement['class']})

                tempList2 = defaultdict(list)

                for errorDesc in errorContent:
                    # Einfügen der Infos zu einem Error-Code in die Liste
                    if len(tempList2[errorCodeID]) >= 3:
                        errorCodeIDNew = errorCodeID + " (1)"
                        tempList2[errorCodeIDNew].append(errorDesc.string)
                    else:
                        tempList2[errorCodeID].append(errorDesc.string)

                for code, value in tempList2.iteritems():
                    tempList['code'] = code
                    for explanation in value:
                        tempList['explanation'] = explanation

            elif(not solutionElement):
                code = errorCodesClass.find(codeElement['elem'], {"class": codeElement['class']})

                explanationList = errorCodesClass.findAll(explanationElement['elem'], {"class": explanationElement['class']})
                explanationTemp = ""

                for explanation in explanationList:
                    explanationTemp = explanationTemp + explanation.getText()

                if (code):
                    tempList['code'] = code.getText()
                if (explanation):
                    tempList['explanation'] = explanationTemp

            else:
                code = errorCodesClass.find(codeElement['elem'], {"class": codeElement['class']})
                explanation = errorCodesClass.find(explanationElement['elem'], {"class": explanationElement['class']})
                solution = errorCodesClass.find(solutionElement['elem'], {"class": solutionElement['class']})

                if (code):
                    tempList['code'] = code.getText()
                if (explanation):
                    tempList['explanation'] = explanation.getText()
                if (solution):
                    tempList['solution'] = solution.getText()

            errorList["errors"].append(tempList)

    errorList["brand"] = brand
    return errorList
