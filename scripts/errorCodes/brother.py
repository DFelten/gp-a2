# coding=utf-8
from pymongo import MongoClient
from bs4 import BeautifulSoup
from collections import defaultdict

#Datenbankzugriff
client = MongoClient('192.168.99.100', 27017)
db = client.items

def GetErrorCodes():
    collection = db.printercollection
    errorList = defaultdict(list)
    entries = collection.find({"html": {"$exists": "true"}})

    # Alle Einträge durchgehen
    for entry in entries:
        soup = BeautifulSoup(entry["html"], 'html.parser')
        errorCodesClasses = soup.findAll("div", {"class": "error_codes"})

        for errorCodesClass in errorCodesClasses:
            # Erzeugen der ID des Error-Codes
            errorCodeID = errorCodesClass.find("p", {"class": "error_codes_content_sml"}).string
            errorCodeID = str(errorCodeID).replace(".", ",")
            # Liste der Infos zu einem Error-Code
            errorContent = errorCodesClass.findAll("p", {"class": "error_codes_content_sml"})

            for errorDesc in errorContent:
                # Einfügen der Infos zu einem Error-Code in die Liste
                if len(errorList[errorCodeID]) >= 3:
                    errorCodeIDNew = errorCodeID + " (1)"
                    errorList[errorCodeIDNew].append(errorDesc.string)
                else:
                    errorList[errorCodeID].append(errorDesc.string)

    # Erzeugen der Liste für die Ausgabe
    errorListFinal = dict((k, tuple(v)) for k, v in errorList.iteritems())

    # Erzeugung der gewollten Struktur aus den gewonnenen Daten
    errorList = defaultdict(list)
    errorList["brand"] = "Brother"

    for key, value in errorListFinal.iteritems():
        tempList = defaultdict(list)
        tempList["code"] = value[0]
        tempList["explanation"] = value[1]
        tempList["solution"] = value[2]
        errorList["errors"].append(tempList)

    return errorList