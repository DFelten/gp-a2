# coding=utf-8
from pymongo import MongoClient
from bs4 import BeautifulSoup
from collections import defaultdict
import pprint

#Datenbankzugriff
client = MongoClient('192.168.99.100', 27017)
db = client.items

def GetErrorCodes():
    collection = db.printercollection
    errorList = defaultdict(list)
    entries = collection.find({"url": "http://www.canon.de/support/consumer_products/product_ranges/printers/pixma/error_codes/"})

    for entry in entries:
        soup = BeautifulSoup(entry["html"], 'html.parser')
        errorCodesClasses = soup.findAll("div", {"class": "c-full-section-listing"})

        for errorCodesClass in errorCodesClasses:
            tempList = defaultdict(list)
            code = errorCodesClass.find("h2", {"class": "c-denda"})
            explanation = errorCodesClass.find("p", {"class": "c-detail-introtext"})
            solution = tempList['solution'] = errorCodesClass.find("div", {"class": "c-detail-listing-img"})

            if (code):
                tempList['code'] = code.getText()
            if (explanation):
                tempList['explanation'] = explanation.getText()
            if (solution):
                tempList['solution'] = solution.getText()

            errorList["errors"].append(tempList)

    errorList["brand"] = "Canon"

    return errorList
