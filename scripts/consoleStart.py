import handler
import pprint
# True fuer Error Codes, False fuer Kontaktdaten
debugError = True

# Crawler laufen lassen?
startCrawler = True
# In Datenbanken eintragen?
insertIntoDatabases = False
# Postprocessing starten?
doPostprocessing = True
# Datenbank leeren?
clearDatabase = False
# Verarbeitung von Error Codes oder/und Kontaktdaten?
errorCodes = True
contacts = False
deleteOldEntries =  True

errorBrand = "canon"
contactOrganisation = "TH-Koeln"

# Leere Strings als Platzhalter
errorUrl = errorArea = errorCodeElement = errorExplanationElement = errorSolutionElement = ""
contactUrl = contactArea = contactNameElement = contactInfoElement= contactPhoneElement = contactEmailElement = ""

if debugError:
    # Aufruf fuer Errorcodes

    # Domain und URL fuer Scrappy
    crawlerDomains = ["dfelten.de"]
    crawlerURLs = ["http://www.dfelten.de"]

    if handler.startErrorCodeProcess(insertIntoDatabases, startCrawler, crawlerDomains, crawlerURLs, deleteOldEntries,
                                     errorBrand, errorUrl, errorArea, errorCodeElement, errorExplanationElement,
                                     errorSolutionElement):
        pprint.pprint("Prozess fehlerfrei durchlaufen")
    else:
        pprint.pprint("Prozess mit Fehlern")
else:
    # Aufruf fuer Kotnaktdaten

    # Domain und URL fuer Scrappy
    crawlerDomains = ["th-koeln.de"]
    crawlerURLs = ["https://www.th-koeln.de/hochschule/kontakt_7672.php"]

    if handler.startContactProcess(insertIntoDatabases, startCrawler, crawlerDomains, crawlerURLs, deleteOldEntries,
                                   contactOrganisation, contactUrl, contactArea, contactNameElement, contactInfoElement,
                                   contactPhoneElement, contactEmailElement):
        pprint.pprint("Prozess fehlerfrei durchlaufen")
    else:
        pprint.pprint("Prozess mit Fehlern")